package lambdasstreams.task1;
@FunctionalInterface
public interface Task1 {
    int getValue(int a, int b, int c);
}
