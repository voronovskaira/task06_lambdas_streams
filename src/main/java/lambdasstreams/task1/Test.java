package lambdasstreams.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;


public class Test {
    private static Logger LOG = LogManager.getLogger(Test.class);

    public static void main(String[] args) {
        Task1 maxValue = (a, b, c) -> Collections.max(Arrays.asList(a, b, c));
        Task1 avgValue = (a, b, c) -> (a + b + c) / 3;
        LOG.info(maxValue.getValue(4,9,10));
        LOG.info(avgValue.getValue(5,3,2));
    }
}
