package lambdasstreams.task4;

import lambdasstreams.task3.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

public class TextParser {
    private static Logger LOG = LogManager.getLogger(Stream.class);
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        String str = scan.nextLine();
        TextParser textParser = new TextParser();
        textParser.countUniqueWords(str);
        textParser.sortUniqueWords(str);
        textParser.wordsOccurrence(str);
        textParser.symbolsOccurrence(str);
    }

    public void countUniqueWords(String str) {
        List<String> list = Arrays.asList(str.split(" "));
        long count = list.stream().distinct().count();
        LOG.info("Number of unique words: " + count);
    }

    public void sortUniqueWords(String str) {
        List<String> list = Arrays.asList(str.split(" "));
        List<String> sortedList = list.stream().distinct().sorted(String::compareTo).collect(Collectors.toList());
        LOG.info("Sorted unique words: " + sortedList);
    }

    public void wordsOccurrence(String str) {
        List<String> list = Arrays.asList(str.split(" "));
        Map<String, Long> countWords = list.stream()
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(Function.identity(), counting()));
        LOG.info("Count words:" + countWords);
    }

    public void symbolsOccurrence(String str) {
        List<Character> chars = str.chars()
                .mapToObj(e -> (char) e)
                .collect(Collectors.toList());
        Map<Character, Long> countWords = chars.stream()
                .collect(Collectors.groupingBy(Function.identity(), counting()));
        LOG.info("Count symbols:" + countWords);
    }
}
