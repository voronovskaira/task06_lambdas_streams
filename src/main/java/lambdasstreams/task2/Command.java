package lambdasstreams.task2;

public interface Command {
    void print(String message);
}
