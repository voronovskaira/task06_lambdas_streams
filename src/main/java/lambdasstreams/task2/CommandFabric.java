package lambdasstreams.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

class CommandFabric {
    private static Logger LOG = LogManager.getLogger(CommandFabric.class);
    private Map<Integer, Command> commandMap = new HashMap<>();

    CommandFabric() {
        commandMap.put(1, message -> LOG.info("I am lambda function, your message:  " + message));
        commandMap.put(2, LOG::info);
        commandMap.put(3, new Command() {
            @Override
            public void print(String message) {
                LOG.info("I am anonymous class , your message :" + message);
            }
        });
        commandMap.put(4, new ObjectCommand());
    }
    Command getCommand(int value) {
        return commandMap.get(value);
    }
}
