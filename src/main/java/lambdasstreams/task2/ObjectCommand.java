package lambdasstreams.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ObjectCommand implements Command {
    private static Logger LOG = LogManager.getLogger(ObjectCommand.class);

    @Override
    public void print(String message) {
        LOG.info("I am object of command class, your message: " + message);
    }
}
