package lambdasstreams.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;


public class Menu {
    private static Logger LOG = LogManager.getLogger(Menu.class);

    private Map<Integer, String> menu = new HashMap<>();

    private void createMenu() {
        menu.put(1, "Lambda function");
        menu.put(2, "Method reference");
        menu.put(3, "Anonymous class");
        menu.put(4, "Object of command class");
        menu.put(5, "Exit");
    }

    public void getMenu() {
        LOG.info("Please choose implementation of command");
        createMenu();
        menu.forEach((key, value) -> LOG.info(key + " - " + value + "\n"));

    }


}
