package lambdasstreams.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Main {
    private static Logger LOG = LogManager.getLogger(CommandFabric.class);
    static Scanner scan = new Scanner(System.in);
    static CommandFabric commandFabric = new CommandFabric();
    static Menu menu = new Menu();

    public static void main(String[] args) {
        menu.getMenu();
        int value = scan.nextInt();
        Command command = commandFabric.getCommand(value);
        LOG.info("Input message");
        scan.nextLine();
        String message = scan.nextLine();
        command.print(message);

    }
}
