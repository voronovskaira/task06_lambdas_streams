package lambdasstreams.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Stream {

    private List<Integer> list = java.util.stream.Stream.generate(new Random()::nextInt).limit(5).collect(Collectors.toList());
    private static Logger LOG = LogManager.getLogger(Stream.class);
    private double average;

    void getMaxNumber() {
        int max = list.stream().mapToInt(x -> x).max().orElse(0);
        LOG.info("Max number is : " + max);
    }

    void getMinNumber() {
        int min = list.stream().mapToInt(x -> x).min().orElse(0);
        LOG.info("Min number is : " + min);
    }

    void getAverageNumber() {
        average = list.stream().mapToInt(x -> x).average().orElse(0);
        LOG.info("Average number is : " + average);
        //return average;
    }

    void getSum() {
        int sum = list.stream().mapToInt(x -> x).sum();
        LOG.info("Sum  is : " + sum);
        int sum2 = list.stream().mapToInt(x -> x).reduce((a, b) -> a + b).orElse(0);
        System.out.println("Sum count using reduce method: " + sum2);
    }

    void getNumberBiggerThanAverage() {
        long n = list.stream().filter(y -> y >= average).count();
        LOG.info("Number of values that are bigger than average is: " + n);
    }

}
