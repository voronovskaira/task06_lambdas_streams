package lambdasstreams.task3;

public class Main {
    public static void main(String[] args) {
        Stream stream = new Stream();
        stream.getMaxNumber();
        stream.getMinNumber();
        stream.getSum();
        stream.getAverageNumber();
        stream.getNumberBiggerThanAverage();
    }
}
